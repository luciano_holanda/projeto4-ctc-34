
public class Fibonacci {
	
	public static long fib(int n){
		 if (n <= 1) return n;
	     else return fib(n-1) + fib(n-2);
	}
	
	public static String zerosForIndex(int index){
		String result = "";
		for(int i = 0; i<fib(index);i++){
			result = result+"0";
		}
		return result;
	}
	
	public static void buildFibonacciString(){
		String fibo = "1010";
		for(int i = 3; i<100; i++){
			fibo = fibo+1+zerosForIndex(i);
			System.out.println(fibo);
			try {
				Thread.sleep(500);//1000 milliseconds is one second.
			} catch(InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		buildFibonacciString();
	}
	
}
