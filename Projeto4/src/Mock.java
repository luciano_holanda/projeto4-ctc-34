

import java.util.ArrayList;

import model.State;
import model.TransferFunction;
import model.TuringMachine;

public class Mock {
	
	
	@SuppressWarnings("unchecked")
	public static TuringMachine generateMockItemA(){
		ArrayList<State> states = new ArrayList<>();
		State q1 = new State("q1",false);
		State q2 = new State("q2",false);
		State q3 = new State("q3",false);
		State q4 = new State("q4",false);
		State q5 = new State("q5",false);
		State q6 = new State("q6",false);
		State q7 = new State("q7",false);
		State q8 = new State("q8",false);
		State q9 = new State("q9",false);
		State q10 = new State("q10",false);
		State q11 = new State("q11",false);
		State q12 = new State("q12",false);
		ArrayList<TransferFunction> transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0", "*","R",q1));
		transfer.add(new TransferFunction("1", "*","R",q2));
		q1.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q1);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","B","R",q2));
		transfer.add(new TransferFunction("1","*","R",q3));
		q2.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q2);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","B","R",q3));
		transfer.add(new TransferFunction("B","1","L",q4));
		q3.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q3);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","*","L",q4));
		transfer.add(new TransferFunction("B","0","R",q5));
		transfer.add(new TransferFunction("1","*","L",q8));
		q4.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q4);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","*","R",q5));
		transfer.add(new TransferFunction("1","*","R",q6));
		q5.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q5);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","*","R",q6));
		transfer.add(new TransferFunction("B","0","L",q7));
		q6.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q6);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","*","L",q7));
		transfer.add(new TransferFunction("1","*","L",q4));
		q7.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q7);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","*","L",q8));
		transfer.add(new TransferFunction("1","*","R",q1));
		transfer.add(new TransferFunction("B","0","R",q9));
		q8.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q8);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","*","R",q9));
		transfer.add(new TransferFunction("1","*","R",q10));
		q9.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q9);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","*","R",q10));
		transfer.add(new TransferFunction("1","*","R",q11));
		q10.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q10);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","*","R",q11));
		transfer.add(new TransferFunction("B","0","L",q12));
		q11.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q11);
		transfer = new ArrayList<>();
		transfer.add(new TransferFunction("0","*","L",q12));
		transfer.add(new TransferFunction("1","*","L",q4));
		q12.setTransferFunctions((ArrayList<TransferFunction>) transfer.clone());
		states.add(q12);
		return new TuringMachine(states, q1);
	}
	
	public static ArrayList<String> generateSymbolsA(){
		ArrayList<String> result = new ArrayList<>();
		result.add("1");
		result.add("0");
		result.add("1");
		result.add("0");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		result.add("B");
		return result;
	}
	
	
}
