package model;

import java.util.ArrayList;

public class State {
	
	private String name;
	private ArrayList<TransferFunction> transferFunctions;
	private boolean isAccepted;
	
	public State(String name,boolean accepted){
		this.name = name;
		this.isAccepted = accepted;
	}
	
	public State(String name, 
			ArrayList<TransferFunction> functions, 
			boolean isAccepted){
		this.name = name;
		this.transferFunctions = functions;
		this.isAccepted = isAccepted;
	}
	
	public boolean isAccepted(){
		return isAccepted;
	}
	
	public ArrayList<TransferFunction> getPossibleTransitions(String symbol){
		ArrayList<TransferFunction> transfers = new ArrayList<>();
		transfers.addAll(getEmptySymbolTransitions());
		for(TransferFunction tf : transferFunctions){
			if(tf.receivedSymbol.equals(symbol)){
				transfers.add(tf);
			}
		}
		return transfers;
	}
	
	public ArrayList<TransferFunction> getEmptySymbolTransitions(){
		ArrayList<TransferFunction> transfers = new ArrayList<>();
		for(TransferFunction tf : transferFunctions){
			if(tf.receivedSymbol.equals("*")){
				transfers.add(tf);
			}
		}
		return transfers;
	}
	
	public void setTransferFunctions(ArrayList<TransferFunction> transfer){
		this.transferFunctions = transfer;
	}
	
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("Estado "+name+":\n\tFun��es de transfer�ncia:\n");
		for(TransferFunction tf : transferFunctions){
			builder.append("\t\t"+tf+"\n");
		}
		if(isAccepted){
			builder.append("\tEstado � de aceita��o");
		}
		return builder.toString();
	}

	public String getName() {
		return name;
	}
	
}
