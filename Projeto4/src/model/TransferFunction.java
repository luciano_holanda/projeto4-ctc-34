package model;

public class TransferFunction {
	
	String receivedSymbol;
	String direction; //R for right, L for left
	String includedSymbol;
	State resultingState;
	
	
	public TransferFunction(String receivedSymbol, String included, String direction,State result){
		this.receivedSymbol = receivedSymbol;
		this.includedSymbol = included;
		this.direction = direction;
		this.resultingState = result;
	}
	
	@Override
	public String toString(){
		return receivedSymbol+"/"+includedSymbol+","+direction+" para Estado: "+resultingState.getName();
	}
}
